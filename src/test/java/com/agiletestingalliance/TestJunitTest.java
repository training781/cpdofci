package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

import com.agiletestingalliance.AboutCPDOF;
import com.agiletestingalliance.Duration;
import com.agiletestingalliance.MinMax;
import com.agiletestingalliance.TestClass;
import com.agiletestingalliance.Usefulness;

public class TestJunitTest {


    @Test
    public void testAboutCPDOF() throws Exception {

      final  String addResult= new AboutCPDOF().desc();
        assertTrue(addResult.equals("CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings."));
        
    }
  
    @Test
    public void testDuration() throws Exception {

      final  String addResult= new Duration().dur();
        assertTrue(addResult.equals("CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams."));
        
    }

    @Test
    public void testMinMaxMax() throws Exception {

      final  int maxNum= new MinMax().fun(1,2);
        assertEquals("Max Number",2,maxNum);
        
    }
    @Test
    public void testMinMaxMin() throws Exception {

      final  int maxNum= new MinMax().fun(2,1);
        assertEquals("Max Number",2,maxNum);
        
    }
    @Test
    public void testMinMaxBar() throws Exception {

      final  String inputString= new MinMax().bar("testString");
      assertTrue(inputString.equals("testString"));
        
    }

    @Test
    public void testTestClass() throws Exception {

      final  String inputString= new TestClass("testString").gstr();
      assertTrue(inputString.equals("testString"));
        
    }

    @Test
    public void testUsefulness() throws Exception {

      final  String inputString= new Usefulness().desc();
      assertTrue(inputString.equals("DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset."));
        
    }
    @Test
    public void testUsefulnessFuncWF() throws Exception {

      new Usefulness().functionWF();
      assertTrue(true);
        
    }

}
